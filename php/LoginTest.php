<?php

class LoginTest {

    private $db_connection = null;
    public $errors = array();
    public $messages = array();

    public function __construct() {
        session_start();
        $_SESSION["logged_in"] = false;
        if (filter_has_var(INPUT_POST, "login")) {
            //echo "Próba zalogowania";
            $this->loginUserWithPostData();
        } else if(filter_has_var(INPUT_POST, "logout")){
            $this->logout();
        }
    }
    
    private function logout(){
        $_SESSION["logged_in"] = false;
        session_destroy();
    }

    private function loginUserWithPostData() {
        $username = filter_input(INPUT_POST, "username");
        $password = filter_input(INPUT_POST, "password");

        if (empty($username) || empty($password)) {

            if (empty($username)) {
                $this->errors[] = "Pusty login";
            }
            if (empty($password)) {
                $this->errors[] = "Puste hasło";
            }
            return;
        }
        $user_data = $this->getUserData($username);
        //echo json_encode($user_data);        
        if (!isset($user_data[0]["id"])) {
            $this->errors[] = "Błąd logowania";
            return;
        }
        if (!password_verify($password, password_hash($user_data[0]["pass"], PASSWORD_DEFAULT))) { //user_data[0]['pass'] - dane dla 1szego rzedu i kolumny pass
            $this->errors[] = "Błąd hasła";
            return;
        } 
        $_SESSION["logged_in"] = true;
        $_SESSION["id"] = $user_data[0]["id"];
        $_SESSION["name"] = $user_data[0]["name"]; //zmienna sesji name przyjmuje wyciagnieta wartosc 1szego rzedu dla kolumny name
    }

    private function databaseConnection() {
        if ($this->db_connection != null) {
            return true;
        } else {
            try {
                $this->db_connection = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';', DB_USER, DB_PASS);
                $this->db_connection->query('SET NAMES utf8');
                $this->db_connection->query('SET CHARACTER_SET utf8_polish_ci');
                return true;
            } catch (PDOException $ex) {
                $this->errors[] = "Connection error: " . $ex->getMessage();
            }
        }
        return false;
    }

    private function getUserData($name) {
        if ($this->databaseConnection()) {
            $query_user = $this->db_connection->prepare('SELECT * FROM users WHERE name = :name');
            $query_user->bindValue(':name', $name, PDO::PARAM_STR);
            $query_user->execute();
            return $query_user->fetchAll();
        } else {
            return false;
        }
    }

}
