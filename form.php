<?php
session_start();

$userinfo = array(
    'user1' => 'password1',
    'user2' => 'password2'
);

if (isset($_GET['logout'])) {
    $_SESSION['username'] = '';
    header('Location:  ' . $_SERVER['PHP_SELF']);
}

if (isset($_POST['username'])) {
    if ($userinfo[$_POST['username']] == $_POST['password']) {
        $_SESSION['username'] = $_POST['username'];
    } else {
        //Invalid Login
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Logowanie</title>
                <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
                <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                    </head>
                    <body>
                        <div class="container">
                            <?php if ($_SESSION['username']): ?>
                                <p>You are logged in as <?= $_SESSION['username'] ?></p>
                                <p><a href="?logout=1" class="btn btn-default" role="button">Logout</a></p>
                            <?php else: ?>
                                <form name="login" action="" method="post">
                                    Username:  <input type="text" name="username" value="" class="form-control"/><br />
                                    Password:  <input type="password" name="password" value="" class="form-control"/><br />
                                    <input type="submit" name="submit" value="Submit" class="btn btn-default"/>
                                </form>
                            <?php endif; ?>
                        </div>
                    </body>
                    </html>