<?php
require_once ("php/config.php");
require_once ("php/LoginTest.php");
$LoginTest = new LoginTest();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">

        <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title>Logowanie z bazą</title>
    </head>
    <body>

        <div class="container">
            <?php
            if ($_SESSION["logged_in"]){
                ?>
            <div class="container">
                <form action="" method="post">
                <div class="row">
                    <div class="col-xs-4">
                        <p>Zalogowany jako <?php echo $_SESSION['name']; ?></p>
                        <button type="submit" name="submit" class="btn btn-danger form-control">Wyloguj</button>
                    </div>
                </div>
                </form>
            </div>
            <?php
            } else {
            ?>
            <div class="row">
                <form action="" method="POST">
                    <div class="col-xs-4">
                        <label>Login</label>
                        <input type="text" name="username" value="" class="form-control"/>
                    </div>
                    <div class="col-xs-4">
                        <label>Hasło</label>
                        <input type="password" name="password" value="" class="form-control"/>
                    </div>
                    <div class="col-xs-4">
                        <label>Kliknij, aby zalogować</label>
                        <button type="submit" name="login" class="btn btn-success form-control">Zaloguj</button>
                    </div> 
                </form>
            </div>
            <?php
            }
            if ($LoginTest->errors) {
                foreach ($LoginTest->errors as $error) {
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="error"><?php echo $error; ?></label>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>

        </div>
    </body>
</html>